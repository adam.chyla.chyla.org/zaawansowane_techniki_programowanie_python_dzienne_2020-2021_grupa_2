# sqlite> select * from ustawienia;
# 1|czy_pierwsze_uruchomienie|0
# sqlite> UPDATE ustawienia SET wartosc=1 WHERE nazwa='czy_pierwsze_uruchomienie';
# sqlite> select * from ustawienia;
# 1|czy_pierwsze_uruchomienie|1

import sqlite3

conn = sqlite3.connect("dane_programu.db")

conn.execute("""
CREATE TABLE IF NOT EXISTS ustawienia (
    id INTEGER PRIMARY KEY,
    nazwa TEXT UNIQUE,
    wartosc INTEGER
);
""")

# ustawienia
# czy_pierwsze_uruchomienie -> 0/1

c = conn.cursor()
c.execute("SELECT wartosc FROM ustawienia WHERE nazwa=?;",
          ("czy_pierwsze_uruchomienie",))
results = c.fetchall() # --> [ (1,) ]  albo  [ ]

if len(results) == 0 or results[0][0] == 1:
    print("uruchomiono pierwszy raz")
    c = conn.cursor()
    c.execute("INSERT OR REPLACE INTO ustawienia VALUES (null, ?, ?)",
              ('czy_pierwsze_uruchomienie', 0))
    conn.commit()
else:
    print("kolejne uruchomienie")

conn.close()
