def druga(nazwa):
    def wewnetrzna():
        print("wewnetrzna")
        nazwa()
        print("koniec")

    return wewnetrzna

# hello = druga(hello)
@druga
def hello():
    print("hello")

hello()
