
def hello(nazwa):
    def wewnetrzna():
        print("hello")
        nazwa()

    return wewnetrzna

# funkcja = hello(funkcja)
@hello
def funkcja():
    print("moja funkcja")


funkcja()
funkcja()
funkcja()

