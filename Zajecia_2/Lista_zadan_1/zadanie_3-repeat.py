class repeat:
	def __init__(self, value, count=None):
		self.value = value
		self.count = count
		self.current = 0
	
	def __next__(self):
		if self.count is not None and self.count == self.current:
			raise StopIteration
		else:
			self.current += 1
			return self.value
	
	def __iter__(self):
		return self

obj = repeat(10, 4)

for x in obj:
	print("x:", x)

obj2 = repeat(10)

for x2 in obj2:
	print("x2:", x2)
