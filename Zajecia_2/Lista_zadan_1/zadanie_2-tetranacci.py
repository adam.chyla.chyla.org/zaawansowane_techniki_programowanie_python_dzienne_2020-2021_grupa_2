
class tetranacci:
    def __init__(self, steps):
        self.wyrazy = [0, 0, 0, 1]
        self.steps = steps
    
    def __next__(self):
        if self.steps > 0:
            suma = sum(self.wyrazy)
            self.wyrazy.append(suma)

            element = self.wyrazy[0]
            del(self.wyrazy[0])

            self.steps -= 1
            return element
        else:
            raise StopIteration

    def __iter__(self):
        return self


obj = tetranacci(6)
obj2 = tetranacci(7)

for x in obj:
    print("obj:", x)
	
for x in obj2:
	print("obj2:", x)
	