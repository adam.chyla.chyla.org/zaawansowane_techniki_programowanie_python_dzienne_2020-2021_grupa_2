# count -> 0, 1, 2, 3, 4, 5

#class count:
#	__init__, __next__, __iter__


def count():
	x = 0
	while True:
		yield x
		x += 1

obj = count()

for x in obj:
	print("for:", x)
