def repeat(obj, times=None):
	x = 0
	while times is None or x < times:
		yield obj
		x += 1

for x in repeat(10, 3):
	print("X:", x)

for i in repeat(10):
	print("i:", i)