def even(x=0):
	if x%2:
		raise ValueError("{} is not even".format(x))
		
	while True:
		yield x
		x += 2
		
for i in even(1):
	print(i)

for i in even(10):
	print(i)
