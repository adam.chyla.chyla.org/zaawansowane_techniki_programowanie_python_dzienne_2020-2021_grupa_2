
class IteratorListy:
	
	def __init__(self, container):
		self.container = container
		self.idx = 0
	
	def __iter__(self):
		return self

	def __next__(self):
		if self.idx < len(self.container):
			t = self.container[self.idx] # jesli istnieje element o takim indeksie
			self.idx += 1
			return t # to go zwracamy
		else:
			raise StopIteration # zatrzymujemy wykonywanie petli for

#        0,  1,    2,   3    4, ... StopIteration
slowa = ['a', 'b', 'c', 'd']

it = IteratorListy(slowa)

for element in it:
	print(element)

# next --> a  idx: 0
# next --> b  idx: 1
# next --> c  idx: 2
# next --> d  idx: 3
# next --> ?  idx: 4