from itertools import islice

class count:
	def __init__(self, start):
		self.value = start
	
	def __iter__(self):
		return self
	
	def __next__(self):
		t = self.value
		self.value += 1
		return t

poczatek = int(input("Podaj liczbe poczatkowa: "))

it = count(poczatek)

for element in islice(it, 0, 10):
  print(element)

