# przyklad na sume trzech silni
# sum(12!, 13!, 14!)

import multiprocessing
import math


def subprocess(results, n):
	value = math.factorial(n)
	results.put(value)

def main():
	results = multiprocessing.Queue()
	processes = [
		multiprocessing.Process(target=subprocess,
								args=(results, 12)),
		multiprocessing.Process(target=subprocess,
								args=(results, 13)),
		multiprocessing.Process(target=subprocess,
								args=(results, 14)),
	]

	for p in processes:
		p.start()
		
	for p in processes:
		p.join()
		
	sum = 0
	while not results.empty():
		item = results.get()
		sum += item
		
	print("Suma:", sum)


if __name__ == "__main__":
	main()
