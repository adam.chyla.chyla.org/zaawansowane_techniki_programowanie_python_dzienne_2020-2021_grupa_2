# przyklad na sume trzech silni
# sum(12!, 13!, 14!)

from concurrent.futures import ProcessPoolExecutor
import math


def main():
	input_data = [12, 13, 14]
	
	with ProcessPoolExecutor(max_workers=2) as pool:
		gen = pool.map(math.factorial, input_data)
	
	print("Suma:", sum(gen))


if __name__ == "__main__":
	main()
