import urllib.request
import threading
import time
import itertools

def download_one_webpage(url):
	with urllib.request.urlopen(url) as f:
		source = f.read().decode('utf-8')
		return source

	
def download_multiple_webpages(urls_to_download):
	all_sources = []
	for url in urls_to_download:
		source = download_one_webpage(url)
		all_sources.append(source)
	return all_sources


def read_urls_from_user(max_urls=5):
	urls_to_download = []
	for _ in range(max_urls):
		url = input("Podaj adres URL (np. https://python.org/): ")
		urls_to_download.append(url)
	return urls_to_download


def print_all_pages_url_and_source_length(urls_to_download, all_sources):
	for url, source in zip(urls_to_download, all_sources):
		print("Liczba znakow dla {}: {}".format(url, len(source)))


def download_pages(urls_to_download, all_sources):
	sources = download_multiple_webpages(urls_to_download)
	all_sources.extend(sources)


def main():
	urls_to_download = read_urls_from_user(max_urls=5)

	# utworzenie watkow
	print("Rozpoczynanie pobierania...")
	all_sources = []
	download_thread = threading.Thread(target=download_pages,
									   args=(urls_to_download,
											all_sources))
	
	download_thread.start()

	animation_character = itertools.cycle('-\|/')
	while download_thread.is_alive():
		print("Oczekiwanie na zakończenie pobierania...", next(animation_character), end='\r')
		time.sleep(0.1)
	
	print("Oczekiwanie na zakończenie pobierania...  ")
	print("Wszystko pobrano.")

	print_all_pages_url_and_source_length(urls_to_download, 
										  all_sources)


if __name__ == "__main__":  # czy uzytkownik uruchomil skrypt
	main()
