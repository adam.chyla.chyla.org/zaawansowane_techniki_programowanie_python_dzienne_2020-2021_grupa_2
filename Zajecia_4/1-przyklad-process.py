import multiprocessing
import time

def count(process_id):
	for i in range(10):
		print(f"Proces {process_id}, i={i}")
		time.sleep(process_id)

t1 = multiprocessing.Process(target=count, args=(1,))
t2 = multiprocessing.Process(target=count, args=(2,))

t1.start()
t2.start()

t1.join()
t2.join()
