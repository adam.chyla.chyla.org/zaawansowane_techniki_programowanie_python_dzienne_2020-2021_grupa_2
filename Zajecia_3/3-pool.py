import threading
import time
from concurrent.futures import ThreadPoolExecutor

suma = 0
suma_lock = threading.Lock()

def count(task_id):
	for i in range(10):
		
		suma_lock.acquire()
		global suma
		suma = suma + i
		print(f"Zadanie {task_id}, i={i}, {suma}")
		suma_lock.release()

		time.sleep(task_id)

with ThreadPoolExecutor(max_workers=2) as executor:
	executor.submit(count, 1)
	executor.submit(count, 2)
	executor.submit(count, 3)
	executor.submit(count, 4)
