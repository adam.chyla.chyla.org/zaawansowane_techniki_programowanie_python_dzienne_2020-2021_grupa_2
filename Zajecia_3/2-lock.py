import threading
import time

suma = 0
suma_lock = threading.Lock()

def count(thread_id,):
	for i in range(10):
		
		suma_lock.acquire()
		global suma
		suma = suma + i
		print(f"Watek {thread_id}, i={i}, {suma}")
		suma_lock.release()

		time.sleep(thread_id)
		
t1 = threading.Thread(target=count, args=(1,))
t2 = threading.Thread(target=count, args=(2,))

t1.start()
t2.start()

t1.join()
t2.join()
