from concurrent.futures import ThreadPoolExecutor
from time import sleep

liczby = [1, 2, 3, 4, 5, 6, 7, 8, 9]

def dodawanie(x):
	return x+10

with ThreadPoolExecutor(max_workers=2) as executor:
	gen = executor.map(dodawanie, liczby)
	nowa = list(gen)

print(nowa)
