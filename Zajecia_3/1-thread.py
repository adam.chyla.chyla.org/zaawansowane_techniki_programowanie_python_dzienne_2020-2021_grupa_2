import threading
import time

def count(thread_id):
	for i in range(10):
		print(f"Watek {thread_id}, i={i}")
		time.sleep(thread_id)

t1 = threading.Thread(target=count, args=(1,))
t2 = threading.Thread(target=count, args=(2,))

t1.start()
t2.start()

t1.join()
t2.join()
