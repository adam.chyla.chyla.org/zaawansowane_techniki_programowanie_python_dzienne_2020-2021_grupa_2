from threading import Thread, Lock
from time import sleep

def count(thread_id, result, lock):
	lock.acquire()
	for i in range(10*thread_id, 10*thread_id+9):
		print(f"Watek {thread_id}: {i}")
		result.append(i)
		
		sleep(thread_id)
	lock.release()


results = []
results_lock = Lock()
		
threads = [
	Thread(target=count, args=(1, results, results_lock)),
	Thread(target=count, args=(2, results, results_lock)),
	Thread(target=count, args=(3, results, results_lock)),
	Thread(target=count, args=(4, results, results_lock)),
]

for t in threads:
	t.start()
	
for t in threads:
	t.join()

print(results)
	