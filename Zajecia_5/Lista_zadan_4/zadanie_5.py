from uuid import uuid4
from hashlib import md5
from concurrent.futures import ProcessPoolExecutor


def generate_uuid4():
    return uuid4().hex


def calculate_hash(text):
    text_bytes = text.encode()
    hash = md5(text_bytes)
    return hash.hexdigest()


def main():
    uuids = [generate_uuid4() for _ in range(200)]

    with ProcessPoolExecutor() as executor:
        results = executor.map(calculate_hash, uuids)

    for uuid, hash in zip(uuids, results):
        print(f"{uuid} - {hash}")


if __name__ == "__main__":
    main()
