import re

# search
text = "Alicja ma kota i psa"

# pattern="k.*?a",

match = re.search(pattern="k[^ ]+a",
                  string=text)
if match:
    print("search: znaleziono:", match.group(0))
else:
    print("search: nie znaleziono")


# match
# czy tekst jest poprawnym kodem pocztowym, XX-XXX, gdzie X to cyfra

text = "12-922"

# pattern="\d\d-\d\d\d"
# pattern="\d{2}-\d{3}$"

match = re.match(pattern="\d{2}-\d{3}",
                 string=text)
if match:
    print("match: kod jest ok")
else:
    print("match: kod nie jest ok")
    
    
# match2
# wybierz liczby XX, YYY z kodu pocztowego XX-YYY, gdzie X,Y to cyfra

text = "12-922asd"

# pattern="\d\d-\d\d\d"
# pattern="\d{2}-\d{3}$"

match = re.match(pattern="(\d{2})-(\d{3})",
                 string=text)
if match:
    print("match2: kod jest ok", match.group(0))
    print("match2: kod jest ok", match.group(1))
    print("match2: kod jest ok", match.group(2))
else:
    print("match2: kod nie jest ok")

    
# findall
# wybierz liczby XX, YYY z kodu pocztowego XX-YYY, gdzie X,Y to cyfra
text = "12-922asd"

elementy_lista = re.findall(pattern="\d+",
                            string=text)
print("findall:", elementy_lista)




