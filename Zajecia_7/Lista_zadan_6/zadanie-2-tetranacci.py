def tetranacci(n):
    if n < 3:
        return 0
    elif n == 3:
        return 1
    else:
        return (tetranacci(n-1) + tetranacci(n-2)
                    + tetranacci(n-3) + tetranacci(n-4))


def test_tetranacci(n, oczekiwana_wartosc):
    wynik = tetranacci(n)
    assert wynik == oczekiwana_wartosc, f"tetranacci({n}): oczekiwano: {oczekiwana_wartosc}; otrzymano: {wynik}"


test_tetranacci(n=-1, oczekiwana_wartosc=0)
test_tetranacci(n=0, oczekiwana_wartosc=0)
test_tetranacci(n=1, oczekiwana_wartosc=0)
test_tetranacci(n=2, oczekiwana_wartosc=0)
test_tetranacci(n=3, oczekiwana_wartosc=1)
test_tetranacci(n=4, oczekiwana_wartosc=1)
test_tetranacci(n=5, oczekiwana_wartosc=2)
test_tetranacci(n=7, oczekiwana_wartosc=8)
test_tetranacci(n=15, oczekiwana_wartosc=1490)
